$(document).ready(function () {
    const BASE_URL = 'http://localhost:8080';
    let filter = {};
    let page = 0;
    let vehicleId;
    let lotId;

    buildSearchForm();
    getTypes();

    $('button.next').click(function () {
        page += 1;
        $('#vehicleContainer').empty();
        getVehiclesFromSearch();
    });

    $('button.previous').click(function () {
        page -= 1;
        $('#vehicleContainer').empty();
        getVehiclesFromSearch();
    });

    $("#search").click(function () {
        getVehiclesFromSearch();
        $('#pagination').removeClass('none')
    });

    $('#makeBid').click(function () {
        event.preventDefault();
        lotId = $('#makeBid').val();
        console.log(lotId);
        makeBid();
    });

    function getVehiclesFromSearch() {
        event.preventDefault();

        filter = {
            "oneFilterVehicleRequests": [
                {
                    "criteriaForSearchVehicle": "BY_TYPE",
                    "firstValue": $("#type option:selected").attr('value'),
                },
                {
                    "criteriaForSearchVehicle": "BY_MAKE",
                    "firstValue": $("#make option:selected").attr('value'),
                },
                {
                    "criteriaForSearchVehicle": "BY_MODEL",
                    "firstValue": $("#model option:selected").attr('value'),
                },
                {
                    "criteriaForSearchVehicle": "BY_LOCATION_COUNTRY",
                    "firstValue": $("#country option:selected").attr('value'),
                },
                {
                    "criteriaForSearchVehicle": "BY_LOCATION_CITY",
                    "firstValue": $("#city option:selected").attr('value'),
                },
                {
                    "criteriaForSearchVehicle": "BY_YEAR_BETWEEN",
                    "firstValue": $("#yearFrom option:selected").attr('value'),
                    "secondValue": $("#yearTo option:selected").attr('value'),
                }
            ]
        };

        console.log(page);

        $.ajax({
            url: BASE_URL + "/vehicle/filter?direction=ASC&page=" + page + "&size=3&sortBy=id",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            headers: {'Access-Control-Allow-Origin': '*'},
            data: JSON.stringify(filter)
        }).done(function (response) {
            event.preventDefault();
            paginationValidation(response);

            $("#vehicleContainer").empty();
            console.log("Response", response);
            for (let i = 0; i < response.data.length; i++) {
                let vehicle = response.data[i];

                $('#vehicleContainer').append('<div class="card">' +
                    '<img src=' + vehicle.imageUrl + ' class="card-img-top" alt="vehicle">' +
                    '<div class="card-body">' +
                    '<h5 class="card-title">' + vehicle.make + ' ' + vehicle.model + ' ' + vehicle.year + '</h5>' +
                    '<p class="card-text">' + vehicle.description + '</p>' +
                    '<div class="card-text characteristics row">' +
                    '<div class="col-6">' +
                    '<p class="mileage">Пробіг: ' + vehicle.odometer + '</p>' +
                    '<p class="location">Локація: ' + vehicle.ownerCountry + ' ' + vehicle.ownerCity + '</p>' +
                    '</div>' +
                    '<div class="col-6">' +
                    '<p class="engine">Двигун: ' + vehicle.capacity + ' ' + vehicle.fuelType + '</p>' +
                    '<p class="transmission">Трансмісія: ' + vehicle.transmissionType + ' ' + vehicle.gear + '</p>' +
                    '</div>' +
                    '<div style="width: 100%;">' +
                    '<button class="more btn btn-primary" value=' + vehicle.id + '>More details</button>' +
                    '<button class="delete btn btn-danger" value=' + vehicle.id + '>Delete</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

            }
            getVehicleId()

            $('.delete').click(function () {
                vehicleId = this.value;
                deleteVehicle();
            });

        }).fail(function (e) {
            console.log("ERROR:\n" + e);
        })
    }

    function deleteVehicle() {
        event.preventDefault();

        $('#search-block').empty();

        $.ajax({
            type: 'DELETE',
            url: BASE_URL + '/vehicle/' + vehicleId,
            contentType: "application/json",
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            getVehiclesFromSearch();
            console.log("OK!!")
        }).fail(function (e) {
            console.error("ERROR", e);
        });
    }

    function getVehicleId() {
        $('.more').click(function () {
            vehicleId = this.value;
            getVehicle();
        });
    }

    function getVehicle() {
        event.preventDefault();

        $('#search-block').empty();

        $.ajax({
            type: 'GET',
            url: BASE_URL + '/vehicle/' + vehicleId,
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            console.log(data);
            $('#vehicle-block').append(
                '<div class="row">' +
                '<h2>Vehicle</h2>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-4"><img src=' + data.imageUrl + '></div>' +
                '<div class="col-8">' +
                '<h2>' + data.make + '(' + data.makeCountry + ') ' + data.model + ' ' + data.year + '</h2>' +
                '<p> Description: ' + data.description + '</p>' +
                '<p> Body: ' + data.body + '</p>' +
                '<p> Mileages: ' + data.odometer + '</p>' +
                '<p> VIN: ' + data.vin + '</p>' +
                '<p> Engine type: ' + data.engineType + ', capacity: ' + data.capacity + ', power: ' + data.enginePower + '</p>' +
                '<p> Transmission type: ' + data.transmissionType + ', gear: ' + data.gear + '</p>' +
                '<p> Features: ' + data.features.map(e => e.feature).join(', ') + '</p>' +
                '<p> Damages: ' + data.damages.map(e => e.typeOfDamage).join(', ') + '</p>' +
                '<p> Owner info: ' + data.owner + ', ' + data.ownerPhone + ', ' + data.ownerCountry + ', ' + data.ownerCity + '</p>' +
                '<div class="bid">' +
                '<p>Current bid: ' + data.currentBid + '</p>' +
                '<form>' +
                '<input  id="bid" type="text" placeholder="Your bid" class="form-control">' +
                '<button id="makeBid" class="btn btn-primary" value=' + data.lotId + '>Make bid</button>' +
                '</form>' +
                '</div>' +
                '</div>' +
                '</div>'
            );

            $('#makeBid').click(function () {
                event.preventDefault();
                lotId = $('#makeBid').val();
                makeBid();
            });

        }).fail(function (e) {
            console.error("ERROR", e);
        });
    }

    function makeBid() {
        event.preventDefault();

        let data = {
            currentBid: $('#bid').val()

        };
        $.ajax({
            url: BASE_URL + '/lot/' + lotId,
            type: "PUT",
            contentType: "application/json",
            headers: {'Access-Control-Allow-Origin': '*'},
            data: JSON.stringify(data)
        }).done(function (response) {
            $('#vehicle-block').empty();
            getVehicle();
            console.log("DONE", response);
        }).fail(function (error) {
            console.log("ERROR", error)
        })

    }

    function paginationValidation(response) {
        if (response.totalEl <= 3) {
            $('.page-item').addClass('disabled');
            $('.page-link').prop('disabled', true);
        } else if (response.page === 0) {
            $('.previous').addClass('disabled');
            $('.previous').prop('disabled', true);
            $('.next').removeClass('disabled');
            $('.next').prop('disabled', false);
        } else if (response.page === Math.floor(response.totalEl / response.size)) {
            $('.next').addClass('disabled');
            $('.next').prop('disabled', true);
            $('.previous').removeClass('disabled');
            $('.previous').prop('disabled', false);
        } else {
            $('.page-item').removeClass('disabled');

        }
    }

    $('#search-form #make').click(function () {
        $('#search-form #model').empty();
        getModels();
        getCities();
    });

    $('#search-form #country').click(function () {
        $('#search-form #city').empty();
        getCities();
    });

    $('#search-form #type').click(function () {
        $('#search-form #make, #search-form #model').empty();
        $('#model').append('<option value="" selected>All</option>');
        getMakes();
        getCities();

    });

    function buildSearchForm() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/vehicle/all',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {

                getMakes();

                let countries = [...new Set(data.map(e => e.ownerCountry))];


                for (let i = 0; i < countries.length; i++) {
                    $('#country').append('<option value=' + countries[i] + '>' + countries[i] + '</option>');
                }

                getCities();


                let currentYear = new Date().getFullYear();
                let minYear = 1950;
                for (let i = 0; i < currentYear - minYear; i++) {
                    $('#yearFrom').append('<option>' + (i + minYear) + '</option>');
                    $('#yearTo').append('<option>' + (currentYear - i) + '</option>');
                }
            }
        ).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getCities() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/location/get_by_country/' + $('#country option:selected').text(),
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {

            let cities = [...new Set(data.map(e => e.city))];

            $('#city').append('<option value="" selected >All</option>');
            for (let i = 0; i < cities.length; i++) {
                $('#city').append('<option value=' + cities[i] + '>' + cities[i] + '</option>');
            }
        }).fail(function (e) {
            console.error("ERROR", e);
        });
    }

    function getTypes() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/type',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                $('#type').append('<option value=' + data[i].vehicleType + '>' + data[i].vehicleType + '</option>');
            }
        }).fail(function (e) {
            console.error("ERROR", e);
        });
    }

    function getMakes() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/form/get_by_type/' + $('#type option:selected').text(),
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            let makes = [...new Set(data.map(e => e.make))];

            $('#make').append('<option value="" selected>All</option>');

            for (let i = 0; i < makes.length; i++) {
                $('#make').append('<option value=' + makes[i] + '>' + makes[i] + '</option>');
            }
        }).fail(function (e) {
            console.error("ERROR", e);
        });
    }

    function getModels() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/form/get_by_make_type/' + $('#make option:selected').text() + '/' + $('#type option:selected').text(),
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {

            let models = [...new Set(data.map(e => e.model))];

            $('#model').append('<option value="" selected >All</option>');
            for (let i = 0; i < models.length; i++) {
                $('#model').append('<option value=' + models[i] + '>' + models[i] + '</option>');
            }
        }).fail(function (e) {
            console.error("ERROR", e);
        });
    }
})
;
