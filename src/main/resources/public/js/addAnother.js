$(document).ready(function () {
    const BASE_URL = 'http://localhost:8080';

    function getCountries() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/country',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {

            for (let i = 0; i < data.length; i++) {
                $('.countryId').append('<option value=' + data[i].id + '>' + data[i].countryName + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        });

    }

    getCountries();
    getMakes();

    $('#addCountryForm').change(function () {
        $('#addMakeForm, #addLocationForm').empty();
        getCountries();
    });

    $('#addMakeForm').change(function () {
        $('#addModelForm').empty();
        getMakes();
    });

    function getMakes() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/make',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {

            for (let i = 0; i < data.length; i++) {
                $('.makeId').append('<option value=' + data[i].id + '>' + data[i].name + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        });

    }


    $('#addBodyForm button').click(function () {
        addBody();
    });

    function addBody() {
        event.preventDefault();

        let body = {
            type: $('#addBodyForm .type').val()
        };

        $.ajax({
            url: BASE_URL + "/body",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(body)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });

    }

    $('#addTypeForm button').click(function () {
        addType();
    });

    function addType() {
        event.preventDefault();

        var type = {
            vehicleType: $('#addTypeForm .vehicleType').val()
        };

        $.ajax({
            url: BASE_URL + "/type",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(type)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }

    $('#addMakeForm button').click(function () {
        addMake();
    });

    function addMake() {
        event.preventDefault();
        getCountries();
        var make = {
            countryId: $('#addMakeForm .countryId').val(),
            name: $('#addMakeForm .name').val()
        };

        $.ajax({
            url: BASE_URL + "/make",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(make)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }

    $('#addLocationForm button').click(function () {
        addLocation();
    });

    function addLocation() {
        event.preventDefault();
        getCountries();

        var location = {
            countryId: $('#addLocationForm .countryId').val(),
            city: $('#addLocationForm .name').val(),
            postalCode: $('#addLocationForm .postalCode').val()
        };

        $.ajax({
            url: BASE_URL + "/location",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(location)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }

    $('#addModelForm button').click(function () {
        addModel();
    });

    function addModel() {
        event.preventDefault();

        getMakes();

        var data = {
            makeId: $('#addModelForm .makeId').val(),
            name: $('#addModelForm .name').val(),
            year: $('#addModelForm .year').val()
        };

        $.ajax({
            url: BASE_URL + "/model",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }

    $('#addEngineForm button').click(function () {
        addEngine();
    });

    function addEngine() {
        event.preventDefault();

        var data = {
            capacity: $('#addEngineForm .capacity').val(),
            cylinder: $('#addEngineForm .cylinder').val(),
            engineType: $('#addEngineForm .engineType').val(),
            fuelType: $('#addEngineForm .fuelType').val(),
            name: $('#addEngineForm .name').val(),
            power: $('#addEngineForm .power').val()
        };

        $.ajax({
            url: BASE_URL + "/engine",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }

    $('#addTransmissionForm button').click(function () {
        addTransmission();
    });

    function addTransmission() {
        event.preventDefault();

        var data = {
            gear: $('#addTransmissionForm .gear').val(),
            type: $('#addTransmissionForm .type').val()
        };

        $.ajax({
            url: BASE_URL + "/transmission",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }


    $('#addDamageForm button').click(function () {
        addDamage();
    });

    function addDamage() {
        event.preventDefault();

        var data = {
            damage: $('#addDamageForm .damage').val()
        };

        $.ajax({
            url: BASE_URL + "/damage",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }


    $('#addFeatureForm button').click(function () {
        addFeature();
    });

    function addFeature() {
        event.preventDefault();

        var data = {
            feature: $('#addFeatureForm .feature').val()
        };

        $.ajax({
            url: BASE_URL + "/feature",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }


    $('#addCountryForm button').click(function () {
        addCountry();
    });

    function addCountry() {
        event.preventDefault();

        var data = {
            abbreviation: $('#addCountryForm .abbreviation').val(),
            countryName: $('#addCountryForm .countryName').val()
        };

        $.ajax({
            url: BASE_URL + "/country",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
        }).fail(function (e) {
            console.log(e);
        });
    }
});