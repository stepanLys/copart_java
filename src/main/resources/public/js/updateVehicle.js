$(document).ready(function () {
    const BASE_URL = 'http://localhost:8080';

    getType();
    getBody();
    getMake();
    getEngine();
    getTransmission();
    getDamage();
    getFeature();
    getLocation();
    getOwner();

    $('#save').click(function () {
        updateVehicle();
    });

    function updateVehicle() {
        event.preventDefault();

        let bodyId = $("#body").children("option:selected").val();
        let engineId = $("#engine").children("option:selected").val();
        let locationId = $("#location").children("option:selected").val();
        let makeId = $("#make").children("option:selected").val();
        let modelId = $("#model").children("option:selected").val();
        let ownerId = $("#owner").children("option:selected").val();
        let transmissionId = $("#transmission").children("option:selected").val();
        let typeId = $("#type").children("option:selected").val();

        let damages = $("#damages").val() || [];
        let features = $("#features").val() || [];

        let vin = $("#VIN").val();
        let description = $("#description").val();
        let odometer = $("#odometer").val();
        let year = $("#year").val();

        let files = document.getElementById("image").files;
        let pictureParam = files.length > 0 ? files[0] : null;

        let fd = new FormData();
        fd.append("bodyId", bodyId);
        fd.append("engineId", engineId);
        fd.append("locationId", locationId);
        fd.append("makeId", makeId);
        fd.append("modelId", modelId);
        fd.append("ownerId", ownerId);
        fd.append("transmissionId", transmissionId);
        fd.append("typeId", typeId);
        fd.append("damages", damages);
        fd.append("features", features);
        fd.append("VIN", vin);
        fd.append("description", description);
        fd.append("odometer", odometer);
        fd.append("year", year);

        fd.append("file", pictureParam);

        console.log(damages);
        console.log(fd);

        $.ajax({
            url: BASE_URL + '/vehicle/' + $('#id').val(),
            type: "PUT",
            processData: false,
            contentType: false,
            data: fd
        }).done(function (response) {
            alert("OK!!");
            console.log(response);
            console.log(BASE_URL + '/vehicle/' + $('#id').val());
        }).fail(function (error) {
            console.log(error)
        })
    }

    $('#updateVehicleForm #id').change(function () {
        getById();
    });

    function getById() {
        $.ajax({
            url: BASE_URL + '/vehicle/' + $('#updateVehicleForm #id').val(),
            type: "GET",
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (response) {
            $('#type option').filter(function () {
                return $(this).text() === response.type;
            }).prop('selected', true);

            $('#make option').filter(function () {
                return $(this).text() === response.make;
            }).prop('selected', true);

            $('#model option').filter(function () {
                return $(this).text() === response.model;
            }).prop('selected', true);

            $('#body option').filter(function () {
                return $(this).text() === response.body;
            }).prop('selected', true);

            $('#engine option').filter(function () {
                let engine = response.engineType + ' ' + response.enginePower + 'hp ' + response.capacity + 'cc';
                return $(this).text() === engine;
            }).prop('selected', true);

            $('#transmission option').filter(function () {
                let transmission = response.transmissionType + ' ' + response.gear + ' gears';
                return $(this).text() === transmission;
            }).prop('selected', true);

            $('#VIN ').val(response.vin);
            $('#odometer ').val(response.odometer);
            $('#year ').val(response.year);
            $('#description ').val(response.description);

            $('#location option').filter(function () {
                let location = response.ownerCountry + ' ' + response.ownerCity;
                return $(this).text() === location;
            }).prop('selected', true);

            $('#owner option').filter(function () {
                let owner = response.owner + ' ' + response.ownerPhone;
                return $(this).text() === owner;
            }).prop('selected', true);

        }).fail(function (error) {
            console.log("ERROR", error);
            $('#updateVehicleForm').empty();
            $('#updateVehicleForm').append('<p style="color: red; font-size: 25px">'+ error.responseJSON.message +'</p>');
            console.log(error)
        })
    }

    $('#make').change(function () {
        $('#model').empty();
        getModel();
    });

    function getBody() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/body',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                $('#body').append('<option value=' + data[i].id + '>' + data[i].type + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getType() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/type',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                $('#type').append('<option value=' + data[i].id + '>' + data[i].vehicleType + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getMake() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/make',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                $('#make').append('<option value=' + data[i].id + '>' + data[i].name + '</option>');
            }
            getModel();
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getLocation() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/location',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                $('#location').append('<option value=' + data[i].id + '>' + data[i].country + " " + data[i].city + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getOwner() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/user',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            console.log(data)
            for (let i = 0; i < data.length; i++) {
                let owner = data[i].name + ' ' + data[i].phone;
                $('#owner').append('<option value=' + data[i].id + '>' + owner + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getModel() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/model',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            let models = [...new Set(data.map(JSON.stringify))].map(JSON.parse);
            for (let i = 0; i < data.length; i++) {
                $('#model').append('<option value=' + models[i].id + '>' + models[i].name + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getEngine() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/engine',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                let engine = data[i].engineType + ' ' + data[i].power + 'hp ' + data[i].capacity + 'cc';
                $('#engine').append('<option value=' + data[i].id + '>' + engine + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getTransmission() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/transmission',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                let transmission = data[i].type + ' ' + data[i].gear + ' gears';

                $('#transmission').append('<option value=' + data[i].id + '>' + transmission + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getDamage() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/damage',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {
            for (let i = 0; i < data.length; i++) {
                $('#damages').append('<option value=' + data[i].id + '>' + data[i].typeOfDamage + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }

    function getFeature() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/feature',
            dataType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        }).done(function (data) {

            for (let i = 0; i < data.length; i++) {
                $('#features').append('<option value=' + data[i].id + '>' + data[i].feature + '</option>');
            }
        }).fail(function (e) {
            alert("ERROR:\n" + e.responseJSON.errors[0].defaultMessage);
        })
    }


});