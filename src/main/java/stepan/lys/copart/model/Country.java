package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Country extends BaseEntity {

    @Column(length = 25, nullable = false)
    private String countryName;

    @Column(length = 5, nullable = false)
    private String abbreviation; // UKR, USA, UK

    @OneToMany(mappedBy = "country")
    private List<Make> makes = new ArrayList<>();

    @OneToMany(mappedBy = "country")
    private Set<Location> locations = new HashSet<>();
}
