package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Type extends BaseEntity {

    @Column(length = 20, nullable = false)
    private String vehicleType;

    @OneToMany(mappedBy = "type")
    private Set<Vehicle> vehicles = new HashSet<>();

}
