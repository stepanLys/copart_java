package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Lot extends BaseEntity {

    @Column(nullable = false)
    private Integer startBid;

    @Column(nullable = false)
    private Integer currentBid;

    private LocalDateTime startDateTime;

    private LocalDateTime finishDateTime;

    @OneToOne(mappedBy = "lot")
    private Vehicle vehicle;

    @ManyToMany(mappedBy = "lots")
    private Set<User> users = new HashSet<>();
}
