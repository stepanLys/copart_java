package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table
public class Body extends BaseEntity{

    @Column(length = 25, nullable = false)
    private String type;

    @OneToMany(mappedBy = "body")
    private Set<Vehicle> vehicles = new HashSet<>();

}
