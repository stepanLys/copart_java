package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Damage extends BaseEntity {

    @Column(length = 25, nullable = false)
    private String damage;

    @ManyToMany(mappedBy = "damages")
    private Set<Vehicle> vehicles;
}
