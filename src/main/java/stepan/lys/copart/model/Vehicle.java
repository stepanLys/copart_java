package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table
public class Vehicle extends BaseEntity {

    @Column(length = 4, nullable = false)
    private String year;

    @Column(length = 20, nullable = false)
    private String VIN;


    @Column(length = 10, nullable = false)
    private String odometer;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String description;

    @ManyToOne
    @JoinColumn(name = "engine_id", nullable = false)
    private Engine engine;

    @ManyToOne
    @JoinColumn(name = "transmission_id", nullable = false)
    private Transmission transmission;

    @ManyToOne
    @JoinColumn(name = "make_id", nullable = false)
    private Make make;

    @ManyToOne
    @JoinColumn(name = "model_id", nullable = false)
    private Model model;

    @ManyToOne
    @JoinColumn(name = "type_id", nullable = false)
//    Type of vehicle. For example car, motorcycle, truck
    private Type type;

    @ManyToOne
    @JoinColumn(name = "body_id", nullable = false)
//    Type of body. For example sedan, coupe, bus
    private Body body;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false)
    private Location location;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User owner;

    @ManyToMany
    @JoinTable(name = "vehicle_damage",
            joinColumns = @JoinColumn(name = "vehicle_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "damage_id", referencedColumnName = "id")
    )
    private Set<Damage> damages;

    @ManyToMany
    @JoinTable(name = "vehicle_feature",
            joinColumns = @JoinColumn(name = "vehicle_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "feature_id", referencedColumnName = "id")
    )
    private Set<Feature> features;

    @OneToOne
    @JoinColumn(name = "lot_id", nullable = false)
    private Lot lot;

    @Column(length = 50,nullable = false)
    private String urlToPicture;
}
