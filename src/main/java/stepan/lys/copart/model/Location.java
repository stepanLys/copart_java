package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor

@Entity
@Table
public class Location extends BaseEntity {

    @Column(length = 25, nullable = false)
    private String city;

    @Column(length = 10, nullable = false)
    private String postalCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    private Country country;

    @OneToMany(mappedBy = "location")
    private Set<Vehicle> vehicles = new HashSet<>();

}
