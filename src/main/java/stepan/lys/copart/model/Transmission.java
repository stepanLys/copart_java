package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Transmission extends BaseEntity {

    @Column(length = 20, nullable = false)
    private String type;

    @Column(length = 15, nullable = false)
    private Integer gear;

    @OneToMany(mappedBy = "transmission")
    private List<Vehicle> vehicles = new ArrayList<>();
}
