package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Make extends BaseEntity {

    @Column(length = 25, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(mappedBy = "make")
    private Set<Model> models = new HashSet<>();

    @OneToMany(mappedBy = "make")
    private Set<Vehicle> vehicles = new HashSet<>();
}
