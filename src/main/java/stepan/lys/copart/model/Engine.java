package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class Engine extends BaseEntity {

    @Column(length = 25, nullable = false)
    private String name;

    @Column(length = 25, nullable = false)
    private String engineType; // V, R, VR, W

    @Column(length = 25, nullable = false)
    private String fuelType;

    @Column(length = 20, nullable = false)
    private Integer cylinder;

    @Column(length = 5, nullable = false)
    private String capacity; // mm3

    @Column(length = 4, nullable = false)
    private Integer power;

    @OneToMany(mappedBy = "engine")
    private List<Vehicle> vehicles = new ArrayList<>();
}
