package stepan.lys.copart.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table
public class Model extends BaseEntity{

    @Column(length = 25, nullable = false)
    private String name;

    @Column(length = 4, nullable = false)
    private String year;

    @ManyToOne
    @JoinColumn(name = "make_id")
    private Make make;

    @OneToMany(mappedBy = "model")
    private Set<Vehicle> vehicles = new HashSet<>();
}
