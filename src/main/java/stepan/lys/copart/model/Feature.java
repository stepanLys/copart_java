package stepan.lys.copart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Feature extends BaseEntity {

    @Column(length = 25, nullable = false)
    private String feature;

    @ManyToMany(mappedBy = "features")
    private Set<Vehicle> vehicles = new HashSet<>();
}
