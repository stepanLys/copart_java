package stepan.lys.copart.exception;

public class WrongUpdateDataException extends Exception {
    public WrongUpdateDataException(String message) {
        super(message);
    }
}
