package stepan.lys.copart.exception;

public class WrongInputDataException extends RuntimeException {
    public WrongInputDataException(String s) {
        super(s);
    }
}
