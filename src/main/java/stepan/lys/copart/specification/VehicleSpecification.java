package stepan.lys.copart.specification;

import org.springframework.data.jpa.domain.Specification;
import stepan.lys.copart.dto.request.FilterVehicleRequest;
import stepan.lys.copart.dto.request.OneFilterVehicleRequest;
import stepan.lys.copart.model.*;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class VehicleSpecification implements Specification<Vehicle> {

    private FilterVehicleRequest filterVehicleRequest;


    public VehicleSpecification(FilterVehicleRequest filterVehicleRequest) {
        this.filterVehicleRequest = filterVehicleRequest;
    }

    private <T extends BaseEntity> Predicate filterByCountry(Root<Vehicle> root, CriteriaBuilder criteriaBuilder,
                                                             OneFilterVehicleRequest oneFilterVehicleRequest, String fieldFromVehicle) {
        Join<Vehicle, T> locationJoin = root.join(fieldFromVehicle).join("country");

        return criteriaBuilder.equal(locationJoin.get("countryName"), oneFilterVehicleRequest.getFirstValue());
    }

    private <T extends BaseEntity> Predicate filterBySomeJoinPropertyBetween(Root<Vehicle> root,
                                                                             CriteriaBuilder criteriaBuilder,
                                                                             OneFilterVehicleRequest oneFilterVehicleRequest,
                                                                             String fieldFromVehicle,
                                                                             String fieldFromJoinedEntity) {
        Join<Vehicle, T> vehicleTJoin = root.join(fieldFromVehicle);
        return criteriaBuilder.between(vehicleTJoin.get(fieldFromJoinedEntity),
                Integer.parseInt(oneFilterVehicleRequest.getFirstValue()),
                Integer.parseInt(oneFilterVehicleRequest.getSecondValue()));
    }

    private <T extends BaseEntity> Predicate filterBySomeJoinProperty(Root<Vehicle> root, CriteriaBuilder criteriaBuilder,
                                                                      OneFilterVehicleRequest oneFilterVehicleRequest,
                                                                      String fieldFromVehicle, String fieldFromJoinedEntity) {

        Join<Vehicle, T> vehicleTJoin = root.join(fieldFromVehicle);
        return criteriaBuilder.equal(vehicleTJoin.get(fieldFromJoinedEntity), oneFilterVehicleRequest.getFirstValue());
    }

    private <T extends BaseEntity> Predicate filterBySomeJoinPropertyLessOrEqualTo(Root<Vehicle> root,
                                                                                   CriteriaBuilder criteriaBuilder,
                                                                                   OneFilterVehicleRequest oneFilterVehicleRequest,
                                                                                   String fieldFromVehicle, String fieldFromJoinedEntity) {

        Join<Vehicle, T> vehicleTJoin = root.join(fieldFromVehicle);
        return criteriaBuilder.lessThanOrEqualTo(vehicleTJoin.get(fieldFromJoinedEntity),
                Integer.parseInt(oneFilterVehicleRequest.getFirstValue()));
    }

    private <T extends BaseEntity> Predicate filterBySomeJoinPropertyGreaterOrEqualTo(Root<Vehicle> root, CriteriaBuilder criteriaBuilder,
                                                                                      OneFilterVehicleRequest oneFilterVehicleRequest,
                                                                                      String fieldFromVehicle, String fieldFromJoinedEntity) {

        Join<Vehicle, T> vehicleTJoin = root.join(fieldFromVehicle);
        return criteriaBuilder.greaterThanOrEqualTo(vehicleTJoin.get(fieldFromJoinedEntity),
                Integer.parseInt(oneFilterVehicleRequest.getFirstValue()));
    }

    private Predicate filterBySomePropertyBetween(Root<Vehicle> root,
                                                  CriteriaBuilder criteriaBuilder,
                                                  OneFilterVehicleRequest oneFilterVehicleRequest,
                                                  String property) {
        if (oneFilterVehicleRequest.getFirstValue() != null && oneFilterVehicleRequest.getSecondValue() != null) {
            return criteriaBuilder.between(root.get(property),
                    oneFilterVehicleRequest.getFirstValue(),
                    oneFilterVehicleRequest.getSecondValue());
        } else {
            return criteriaBuilder.conjunction();
        }
    }

    private Predicate filterBySomePropertyLessThatOrEqualTo(Root<Vehicle> root,
                                                            CriteriaBuilder criteriaBuilder,
                                                            OneFilterVehicleRequest oneFilterVehicleRequest,
                                                            String property) {
        if (oneFilterVehicleRequest.getFirstValue() != null && oneFilterVehicleRequest.getSecondValue() != null) {
            return criteriaBuilder.lessThanOrEqualTo(root.get(property), oneFilterVehicleRequest.getFirstValue());
        } else {
            return criteriaBuilder.conjunction();
        }
    }

    private Predicate filterBySomePropertyGreaterThatOrEqualTo(Root<Vehicle> root,
                                                               CriteriaBuilder criteriaBuilder,
                                                               OneFilterVehicleRequest oneFilterVehicleRequest,
                                                               String property) {
        if (oneFilterVehicleRequest.getFirstValue() != null && oneFilterVehicleRequest.getSecondValue() != null) {
            return criteriaBuilder.greaterThanOrEqualTo(root.get(property), oneFilterVehicleRequest.getFirstValue());
        } else {
            return criteriaBuilder.conjunction();
        }
    }


    private Predicate createFilter(Root<Vehicle> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        List<Predicate> makes = new ArrayList<>();
        List<Predicate> models = new ArrayList<>();

        filterVehicleRequest.getOneFilterVehicleRequests().forEach(oneFilterVehicleRequest -> {
            switch (oneFilterVehicleRequest.getCriteriaForSearchVehicle()) {
                case BY_BODY: {
                    predicates.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "body", "type"));
                    break;
                }
                case BY_MAKE: {
                    if (oneFilterVehicleRequest.getFirstValue().length() > 0) {
                        makes.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "make", "name"));

                    }
                    break;
                }
                case BY_MODEL: {
                    if (oneFilterVehicleRequest.getFirstValue().length() > 0) {
                        models.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "model", "name"));
                    }
                    break;
                }

                case BY_TYPE: {
                    predicates.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "type", "vehicleType"));
                    break;
                }
                case BY_DAMAGES: {
                    predicates.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "damages", "damage"));
                    break;
                }
                case BY_FEATURES: {
                    predicates.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "features", "feature"));
                    break;
                }

                case BY_YEAR_BETWEEN: {
                    predicates.add(filterBySomePropertyBetween(root, criteriaBuilder, oneFilterVehicleRequest, "year"));
                    break;
                }
                case BY_YEAR_LESS_THAT: {
                    predicates.add(filterBySomePropertyLessThatOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "year"));
                    break;
                }
                case BY_YEAR_GREATER_THAT: {
                    predicates.add(filterBySomePropertyGreaterThatOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "year"));
                    break;
                }

                case BY_POWER_BETWEEN: {
                    predicates.add(filterBySomeJoinPropertyBetween(root, criteriaBuilder, oneFilterVehicleRequest, "engine", "power"));
                    break;
                }
                case BY_POWER_LESS_THAT: {
                    predicates.add(filterBySomeJoinPropertyLessOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "engine", "power"));
                    break;
                }
                case BY_POWER_GREATER_THAT: {
                    predicates.add(filterBySomeJoinPropertyGreaterOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "engine", "power"));
                    break;
                }

                case BY_MILEAGE_BETWEEN: {
                    predicates.add(filterBySomePropertyBetween(root, criteriaBuilder, oneFilterVehicleRequest, "odometer"));
                    break;
                }
                case BY_MILEAGE_LESS_THAT: {
                    predicates.add(filterBySomePropertyLessThatOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "odometer"));
                    break;
                }
                case BY_MILEAGE_GREATER_THAT: {
                    predicates.add(filterBySomePropertyGreaterThatOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "odometer"));
                    break;
                }

                case BY_MAKE_COUNTRY: {
                    predicates.add(filterByCountry(root, criteriaBuilder, oneFilterVehicleRequest, "make"));
                    break;
                }

                case BY_LOCATION_COUNTRY: {
                    predicates.add(filterByCountry(root, criteriaBuilder, oneFilterVehicleRequest, "location"));
                    break;
                }
                case BY_LOCATION_CITY: {
                    if (oneFilterVehicleRequest.getFirstValue().length() > 0) {
                        predicates.add(filterBySomeJoinProperty(root, criteriaBuilder, oneFilterVehicleRequest, "location", "city"));
                    }
                    break;
                }

                case BY_CAPACITY_BETWEEN: {
                    predicates.add(filterBySomeJoinPropertyBetween(root, criteriaBuilder, oneFilterVehicleRequest, "engine", "capacity"));
                    break;
                }
                case BY_CAPACITY_LESS_THAT: {
                    predicates.add(filterBySomeJoinPropertyLessOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "engine", "capacity"));
                    break;
                }
                case BY_CAPACITY_GREATER_THAT: {
                    predicates.add(filterBySomeJoinPropertyGreaterOrEqualTo(root, criteriaBuilder, oneFilterVehicleRequest, "engine", "capacity"));
                    break;
                }
            }

        });

        if (!makes.isEmpty()) {
            predicates.add(criteriaBuilder.or(makes.toArray(new Predicate[makes.size()])));
        }
        if (!models.isEmpty()) {
            predicates.add(criteriaBuilder.or(models.toArray(new Predicate[models.size()])));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }


    @Override
    public Predicate toPredicate(Root<Vehicle> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return createFilter(root, criteriaBuilder);
    }
}
