package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Type;

@Getter
@Setter
public class TypeResponse {

    private Long id;

    private String vehicleType;

    public TypeResponse(Type type) {
        this.id = type.getId();
        this.vehicleType = type.getVehicleType();
    }
}
