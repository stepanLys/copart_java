package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Feature;

@Getter
@Setter
public class FeatureResponse {

    private Long id;

    private String feature;

    public FeatureResponse(Feature feature) {
        this.id = feature.getId();
        this.feature = feature.getFeature();
    }
}
