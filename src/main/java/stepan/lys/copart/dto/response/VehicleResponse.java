package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Vehicle;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class VehicleResponse {

    private Long id;

    private String year;

    private Integer currentBid;

    private Long lotId;

    private String VIN;

    private String odometer;

    private String description;

    // engine properties
    private String capacity;

    private String engineType;

    private Integer enginePower;

    private String fuelType;
    // engine properties end

    private Integer gear;

    private String transmissionType;

    private String make;

    private String makeCountry;

    private String model;

    //    Type of vehicle. For example car, motorcycle, truck
    private String type;

    //    Type of body. For example sedan, coupe, bus
    private String body;

    private String ownerCity;

    private String ownerCountry;

    private String owner;

    private String ownerPhone;

    private List<DamageResponse> damages;

    private List<FeatureResponse> features;

    private String imageUrl;


    public VehicleResponse(Vehicle vehicle) {
        this.id = vehicle.getId();
        this.year = vehicle.getYear();
        this.VIN = vehicle.getVIN();
        this.odometer = vehicle.getOdometer();
        this.description = vehicle.getDescription();

        this.capacity = vehicle.getEngine().getCapacity();
        this.engineType = vehicle.getEngine().getEngineType();
        this.enginePower = vehicle.getEngine().getPower();
        this.fuelType = vehicle.getEngine().getFuelType();

        this.gear = vehicle.getTransmission().getGear();
        this.transmissionType = vehicle.getTransmission().getType();

        this.make = vehicle.getMake().getName();
        this.makeCountry = vehicle.getMake().getCountry().getCountryName();
        this.model = vehicle.getModel().getName();
        this.type = vehicle.getType().getVehicleType();
        this.body = vehicle.getBody().getType();
        this.ownerCity = vehicle.getLocation().getCity();
        this.ownerCountry = vehicle.getLocation().getCountry().getCountryName();
        this.owner = vehicle.getOwner().getName();
        this.ownerPhone = vehicle.getOwner().getPhone();
        this.damages = vehicle.getDamages()
                .stream()
                .map(DamageResponse::new)
                .collect(Collectors.toList());
        this.features = vehicle.getFeatures()
                .stream()
                .map(FeatureResponse::new)
                .collect(Collectors.toList());

        this.imageUrl = vehicle.getUrlToPicture();

        this.currentBid = vehicle.getLot().getCurrentBid();
        this.lotId = vehicle.getLot().getId();
    }
}
