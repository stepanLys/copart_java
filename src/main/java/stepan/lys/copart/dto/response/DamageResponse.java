package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Damage;

@Getter
@Setter
public class DamageResponse {

    private Long id;

    private String typeOfDamage;

    public DamageResponse(Damage damage) {
        this.id = damage.getId();
        this.typeOfDamage = damage.getDamage();
    }
}
