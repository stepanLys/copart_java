package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Lot;
import stepan.lys.copart.model.User;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter

public class LotResponse {

    private Long id;

    private Integer currentBid;

    private Integer startBid;

    private LocalDateTime startDateTime;

    private LocalDateTime finishDateTime;

    private String vehicle;

    private Set<UserResponse> users;

    public LotResponse(Lot lot) {
        this.id = lot.getId();
        this.currentBid = lot.getCurrentBid();
        this.startBid = lot.getStartBid();
        this.startDateTime = lot.getStartDateTime();
        this.finishDateTime = lot.getFinishDateTime();
        this.vehicle = lot.getVehicle().getMake().getName() + " "
                + lot.getVehicle().getModel().getName();
        this.users = lot.getUsers()
                .stream()
                .map(UserResponse::new)
                .collect(Collectors.toSet());
    }
}
