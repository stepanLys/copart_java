package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Model;

import java.time.LocalDate;

@Getter
@Setter
public class ModelResponse {

    private Long id;

    private String name;

    private String year;

    private String make;

    public ModelResponse(Model model) {
        this.id = model.getId();
        this.name = model.getName();
        this.year = model.getYear();
        this.make = model.getMake().getName();
    }
}
