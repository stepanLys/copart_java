package stepan.lys.copart.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Engine;

@Getter
@Setter
@AllArgsConstructor
public class EngineResponse {

    private Long id;

    private String name;

    private String engineType;

    private String fuelType;

    private Integer cylinder;

    private String capacity;

    private Integer power;

    public EngineResponse(Engine engine) {
        this.id = engine.getId();
        this.name = engine.getName();
        this.engineType = engine.getEngineType();
        this.fuelType = engine.getFuelType();
        this.cylinder = engine.getCylinder();
        this.capacity = engine.getCapacity();
        this.power = engine.getPower();
    }
}
