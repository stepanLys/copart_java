package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Make;
import stepan.lys.copart.model.Model;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class MakeResponse {

    private Long id;

    private String name;

    private String country;

    private List<ModelResponse> models;

    public MakeResponse(Make make) {
        this.id = make.getId();
        this.name = make.getName();
        this.country = make.getCountry().getCountryName();
        this.models = make.getModels()
                .stream()
                .map(ModelResponse::new)
                .collect(Collectors.toList());
    }
}
