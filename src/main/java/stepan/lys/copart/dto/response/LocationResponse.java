package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Location;

@Getter
@Setter
public class LocationResponse {

    private Long id;

    private String city;

    private String postalCode;

    private String country;

    public LocationResponse(Location location) {
        this.id = location.getId();
        this.city = location.getCity();
        this.postalCode = location.getPostalCode();
        this.country = location.getCountry().getCountryName();
    }
}
