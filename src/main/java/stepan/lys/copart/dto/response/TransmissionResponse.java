package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Transmission;

@Getter
@Setter
public class TransmissionResponse {

    private Long id;

    private String type;

    private Integer gear;

    public TransmissionResponse(Transmission transmission) {
        this.id = transmission.getId();
        this.type = transmission.getType();
        this.gear = transmission.getGear();
    }
}
