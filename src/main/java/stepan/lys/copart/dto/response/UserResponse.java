package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.User;

import javax.validation.constraints.Email;
import java.util.Set;

@Getter
@Setter
public class UserResponse {

    private Long id;

    private String name;

    private String surname;

    @Email
    private String email;

    private String password;

    private String phone;

    public UserResponse(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.phone = user.getPhone();
    }
}
