package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Body;

@Getter
@Setter

public class BodyResponse {

    private Long id;

    private String type;

    public BodyResponse(Body body) {
        this.id = body.getId();
        this.type = body.getType();
    }
}
