package stepan.lys.copart.dto.response;

import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Country;

@Getter
@Setter

public class CountryResponse {

    private Long id;

    private String countryName;

    private String abbreviation; // UKR, USA, UK

    public CountryResponse(Country country) {
        this.id = country.getId();
        this.countryName = country.getCountryName();
        this.abbreviation = country.getAbbreviation();
    }
}
