package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class BodyRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String type;
}
