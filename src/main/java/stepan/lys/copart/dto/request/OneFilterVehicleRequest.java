package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OneFilterVehicleRequest {

    private CriteriaForSearchVehicle criteriaForSearchVehicle;

    private String firstValue;

    private String secondValue;
}
