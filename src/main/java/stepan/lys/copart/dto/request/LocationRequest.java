package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class LocationRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String city;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 10)
    private String postalCode;

    @Min(value = 1)
    private Long countryId;
}
