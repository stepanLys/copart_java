package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class FilterVehicleRequest {

    List<OneFilterVehicleRequest> oneFilterVehicleRequests = new ArrayList<>();

}
