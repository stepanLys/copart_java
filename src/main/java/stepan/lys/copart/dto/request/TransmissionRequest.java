package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class TransmissionRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String type;

    @Min(value = 1)
    @Max(value = 15)
    private Integer gear;
}
