package stepan.lys.copart.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String name;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String surname;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    @Email
    private String email;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String password;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String phone;

    @JsonIgnore
    private Long lotId;

}
