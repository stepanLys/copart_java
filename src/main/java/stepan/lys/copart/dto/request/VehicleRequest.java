package stepan.lys.copart.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Damage;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class VehicleRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 4)
    private String year;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 20)
    private String VIN;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 10)
    private String odometer;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 300)
    private String description;

    @Min(value = 1)
    private Long engineId;
    @Min(value = 1)
    private Long transmissionId;
    @Min(value = 1)
    private Long makeId;
    @Min(value = 1)
    private Long modelId;

    //    Type of vehicle. For example car, motorcycle, truck
    @Min(value = 1)
    private Long typeId;

    //    Type of body. For example sedan, coupe, bus
    @Min(value = 1)
    private Long bodyId;
    @Min(value = 1)
    private Long locationId;
    @Min(value = 1)
    private Long ownerId;

    private Set<Long> damages;

    private Set<Long> features;

    @Min(value = 1)
    private Integer startBid;
}
