package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CountryRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String countryName;

    @NotNull
    @NotBlank
    @Size(min = 2, max = 5)
    private String abbreviation; // UKR, USA, UK
}
