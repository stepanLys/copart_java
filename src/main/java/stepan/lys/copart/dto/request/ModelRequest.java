package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ModelRequest {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 30)
    private String name;

    @Min(value = 1990)
    private String year;

    private Long makeId;

}
