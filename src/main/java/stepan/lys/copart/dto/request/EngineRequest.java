package stepan.lys.copart.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter

public class EngineRequest {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String name;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String engineType;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 30)
    private String fuelType;

    @Max(value = 20)
    @Min(value = 1)
    private Integer cylinder;

    @NotNull
    @NotBlank
    @Size(min = 2, max = 5)
    private String capacity;

    @Max(value = 2000)
    @Min(value = 1)
    private Integer power;

}
