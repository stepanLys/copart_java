package stepan.lys.copart.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import stepan.lys.copart.model.Vehicle;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Getter
@Setter
public class LotRequest {

    @Min(value = 1)
    private Integer currentBid;
    @Min(value = 1)
    private Integer startBid;
    @Min(value = 1)
    @Max(value = 60)
    private Integer dayDuration;

    @JsonIgnore
    private LocalDateTime finishDateTime;

}
