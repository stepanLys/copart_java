package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import stepan.lys.copart.dto.request.FilterVehicleRequest;
import stepan.lys.copart.dto.request.VehicleRequest;
import stepan.lys.copart.dto.response.DataResponse;
import stepan.lys.copart.dto.response.VehicleResponse;
import stepan.lys.copart.service.VehicleService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/vehicle")
public class VehicleController {
    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping("/filter")
    public ResponseEntity<DataResponse<VehicleResponse>> filter(@RequestBody FilterVehicleRequest filterVehicleRequest,
                                                                @RequestParam Integer page, @RequestParam Integer size,
                                                                @RequestParam(defaultValue = "id") String sortBy, @RequestParam Sort.Direction direction) {
        return new ResponseEntity<>(
                vehicleService.filter(filterVehicleRequest, page, size, sortBy, direction),
                HttpStatus.OK
        );
    }

    @PostMapping("/filter2")
    public ResponseEntity<List<VehicleResponse>> filter(@RequestBody FilterVehicleRequest filterVehicleRequest) {
        return new ResponseEntity<>(
                vehicleService.filter(filterVehicleRequest),
                HttpStatus.OK
        );
    }

    @GetMapping("/all")
    public ResponseEntity<List<VehicleResponse>> getAll() {
        return new ResponseEntity<>(
                vehicleService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping()
    public ResponseEntity<DataResponse<VehicleResponse>> getAll(@RequestParam Integer page, @RequestParam Integer size,
                                                                @RequestParam String sortBy, @RequestParam Sort.Direction direction,
                                                                @RequestParam(required = false) String name) {
        return new ResponseEntity<>(
                vehicleService.getAll(page, size, sortBy, direction, name),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<VehicleResponse> getById(@PathVariable Long id) {
        return new ResponseEntity<>(
                vehicleService.getById(id),
                HttpStatus.OK
        );
    }


    @PostMapping
    public VehicleResponse save(@Valid VehicleRequest vehicleRequest, @RequestParam MultipartFile file) throws IOException {

        return vehicleService.save(vehicleRequest, file);
    }

    @PutMapping("/{id}")
    public VehicleResponse update(@PathVariable Long id, @Valid VehicleRequest vehicleRequest, @RequestParam MultipartFile file) throws IOException {
        return vehicleService.update(id, vehicleRequest, file);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        vehicleService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
