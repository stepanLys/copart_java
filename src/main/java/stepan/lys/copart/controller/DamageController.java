package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.DamageRequest;
import stepan.lys.copart.dto.response.DamageResponse;
import stepan.lys.copart.service.DamageService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/damage")
public class DamageController {

    private final DamageService damageService;

    @Autowired
    public DamageController(DamageService damageService) {
        this.damageService = damageService;
    }

    @GetMapping()
    public ResponseEntity<List<DamageResponse>> getAll() {
        return new ResponseEntity<>(
                damageService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<DamageResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                damageService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid DamageRequest damageRequest) {
        damageService.save(damageRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid DamageRequest damageRequest) {
        damageService.update(id, damageRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        damageService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
