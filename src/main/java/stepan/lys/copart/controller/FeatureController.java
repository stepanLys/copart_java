package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.FeatureRequest;
import stepan.lys.copart.dto.response.FeatureResponse;
import stepan.lys.copart.service.FeatureService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/feature")
public class FeatureController {

    private final FeatureService featureService;

    @Autowired
    public FeatureController(FeatureService featureService) {
        this.featureService = featureService;
    }

    @GetMapping()
    public ResponseEntity<List<FeatureResponse>> getAll() {
        return new ResponseEntity<>(
                featureService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<FeatureResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                featureService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid FeatureRequest featureRequest) {
        featureService.save(featureRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid FeatureRequest featureRequest) {
        featureService.update(id, featureRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        featureService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
