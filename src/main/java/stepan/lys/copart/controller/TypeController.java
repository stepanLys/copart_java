package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.TypeRequest;
import stepan.lys.copart.dto.response.TypeResponse;
import stepan.lys.copart.service.TypeService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/type")
public class TypeController {

    private final TypeService typeService;

    @Autowired
    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

    @GetMapping()
    public ResponseEntity<List<TypeResponse>> getAll() {
        return new ResponseEntity<>(
                typeService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<TypeResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                typeService.getById(id),
                HttpStatus.OK
        );
    }

    @GetMapping("/by_body/{body}")
    public ResponseEntity<?> getById(@PathVariable String body) {
        return new ResponseEntity<>(
                typeService.findAllByVehicles_Empty_Body(body),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid TypeRequest typeRequest) {
        typeService.save(typeRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid TypeRequest typeRequest) {
        typeService.update(id, typeRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        typeService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
