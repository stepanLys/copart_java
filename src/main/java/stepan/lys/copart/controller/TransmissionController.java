package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.TransmissionRequest;
import stepan.lys.copart.dto.response.TransmissionResponse;
import stepan.lys.copart.service.TransmissionService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/transmission")
public class TransmissionController {

    private final TransmissionService transmissionService;

    @Autowired
    public TransmissionController(TransmissionService transmissionService) {
        this.transmissionService = transmissionService;
    }

    @GetMapping()
    public ResponseEntity<List<TransmissionResponse>> getAll() {
        return new ResponseEntity<>(
                transmissionService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransmissionResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                transmissionService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid TransmissionRequest transmissionRequest) {
        transmissionService.save(transmissionRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid TransmissionRequest transmissionRequest) {
        transmissionService.update(id, transmissionRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        transmissionService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
