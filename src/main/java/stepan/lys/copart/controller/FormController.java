package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.response.ModelResponse;
import stepan.lys.copart.dto.response.VehicleResponse;
import stepan.lys.copart.service.VehicleService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/form")
public class FormController {
    private final VehicleService vehicleService;

    @Autowired
    public FormController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("/get_by_type/{type}")
    public ResponseEntity<List<VehicleResponse>> getByType(@PathVariable String type) {
        return new ResponseEntity<>(
                vehicleService.getByType(type),
                HttpStatus.OK
        );
    }

    @GetMapping("/get_by_make_type/{make}/{type}")
    public ResponseEntity<List<VehicleResponse>> getVehicleByMakeAndType(@PathVariable String make, @PathVariable String type) {
        return new ResponseEntity<>(vehicleService.getVehicleResponseByMakeAndType(make, type), HttpStatus.OK);
    }

    @GetMapping("/get_by_make/{makeId}")
    public ResponseEntity<List<ModelResponse>> getVehicleByMakeAndType(@PathVariable Long makeId) {
        return new ResponseEntity<>(vehicleService.getModelResponseByMake(makeId), HttpStatus.OK);
    }

    @GetMapping("/get_model_by_make_type/{make}/{type}")
    public ResponseEntity<List<ModelResponse>> getModelByMakeAndType(@PathVariable String make, @PathVariable String type) {
        return new ResponseEntity<>(vehicleService.getModelResponseByMakeAndType(make, type), HttpStatus.OK);
    }

}
