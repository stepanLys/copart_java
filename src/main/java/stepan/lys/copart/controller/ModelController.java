package stepan.lys.copart.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.ModelRequest;
import stepan.lys.copart.dto.response.ModelResponse;
import stepan.lys.copart.service.ModelService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/model")
public class ModelController {

    private final ModelService modelService;

    @Autowired
    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping()
    public ResponseEntity<List<ModelResponse>> getAll() {
        return new ResponseEntity<>(
                modelService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<ModelResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                modelService.getById(id),
                HttpStatus.OK
        );
    }

    @GetMapping("/by_make/{make}")
    public ResponseEntity<List<ModelResponse>> getById(@PathVariable String make) {
        return new ResponseEntity<>(
                modelService.findByMake(make),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid ModelRequest modelRequest) {
        modelService.save(modelRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid ModelRequest modelRequest) {
        modelService.update(id, modelRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        modelService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
