package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.MakeRequest;
import stepan.lys.copart.dto.response.MakeResponse;
import stepan.lys.copart.service.MakeService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/make")
public class MakeController {

    private final MakeService makeService;

    @Autowired
    public MakeController(MakeService makeService) {
        this.makeService = makeService;
    }

    @GetMapping()
    public ResponseEntity<List<MakeResponse>> getAll() {
        return new ResponseEntity<>(
                makeService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<MakeResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                makeService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid MakeRequest makeRequest) {
        makeService.save(makeRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid MakeRequest makeRequest) {
        makeService.update(id, makeRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        makeService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
