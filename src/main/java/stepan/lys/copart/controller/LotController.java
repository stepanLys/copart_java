package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.LotRequest;
import stepan.lys.copart.dto.response.LotResponse;
import stepan.lys.copart.service.LotService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/lot")
public class LotController {

    private final LotService lotService;

    @Autowired
    public LotController(LotService lotService) {
        this.lotService = lotService;
    }

    @GetMapping()
    public ResponseEntity<List<LotResponse>> getAll() {
        return new ResponseEntity<>(
                lotService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<LotResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                lotService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid LotRequest lotRequest) {
        lotService.save(lotRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid LotRequest lotRequest) {
        lotService.update(id, lotRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        lotService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
