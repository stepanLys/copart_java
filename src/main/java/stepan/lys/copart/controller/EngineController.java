package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.EngineRequest;
import stepan.lys.copart.dto.response.EngineResponse;
import stepan.lys.copart.service.EngineService;
import stepan.lys.copart.service.impl.EngineServiceImpl;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/engine")
public class EngineController {

    private final EngineService engineService;

    @Autowired
    public EngineController(EngineServiceImpl engineService) {
        this.engineService = engineService;
    }

    @GetMapping()
    public ResponseEntity<List<EngineResponse>> getAll() {
        return new ResponseEntity<>(
                engineService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<EngineResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                engineService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid EngineRequest engineRequest) {
        engineService.save(engineRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid EngineRequest engineRequest) {
        engineService.update(id, engineRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        engineService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
