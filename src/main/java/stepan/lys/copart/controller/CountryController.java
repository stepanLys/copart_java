package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.CountryRequest;
import stepan.lys.copart.dto.response.CountryResponse;
import stepan.lys.copart.service.CountryService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/country")
public class CountryController {

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping()
    public ResponseEntity<List<CountryResponse>> getAll() {
        return new ResponseEntity<>(
                countryService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<CountryResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                countryService.getById(id),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid CountryRequest countryRequest) {
        countryService.save(countryRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid CountryRequest countryRequest) {
        countryService.update(id, countryRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        countryService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
