package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.BodyRequest;
import stepan.lys.copart.dto.response.BodyResponse;
import stepan.lys.copart.service.BodyService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/body")
public class BodyController {

    private final BodyService bodyService;

    @Autowired
    public BodyController(BodyService bodyService) {
        this.bodyService = bodyService;
    }

    @GetMapping()
    public ResponseEntity<List<BodyResponse>> getAll() {
        return new ResponseEntity<>(bodyService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BodyResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(bodyService.getById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid BodyRequest bodyRequest) {
        bodyService.save(bodyRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid BodyRequest bodyRequest) {
        bodyService.update(id, bodyRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        bodyService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
