package stepan.lys.copart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import stepan.lys.copart.dto.request.LocationRequest;
import stepan.lys.copart.dto.response.LocationResponse;
import stepan.lys.copart.service.LocationService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/location")
public class LocationController {

    private final LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping()
    public ResponseEntity<List<LocationResponse>> getAll() {
        return new ResponseEntity<>(
                locationService.getAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<LocationResponse> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(
                locationService.getById(id),
                HttpStatus.OK
        );
    }

    @GetMapping("/get_by_country/{country}")
    public ResponseEntity<List<LocationResponse>> getByCountry(@PathVariable("country") String country) {
        return new ResponseEntity<>(
                locationService.getByCountry(country),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid LocationRequest locationRequest) {
        locationService.save(locationRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable() Long id, @RequestBody @Valid LocationRequest locationRequest) {
        locationService.update(id, locationRequest);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        locationService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
