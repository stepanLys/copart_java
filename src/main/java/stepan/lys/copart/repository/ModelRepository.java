package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.dto.response.ModelResponse;
import stepan.lys.copart.model.Model;

import java.util.List;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long> {

    List<Model> findAllByMake_Name(String make);
}
