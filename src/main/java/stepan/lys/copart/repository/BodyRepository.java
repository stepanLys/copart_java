package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Body;

@Repository
public interface BodyRepository extends JpaRepository<Body, Long> {

}
