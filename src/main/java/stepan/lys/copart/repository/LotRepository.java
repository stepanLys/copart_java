package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Lot;

@Repository
public interface LotRepository extends JpaRepository<Lot, Long> {
}
