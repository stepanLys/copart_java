package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Damage;

@Repository
public interface DamageRepository extends JpaRepository<Damage, Long> {
}
