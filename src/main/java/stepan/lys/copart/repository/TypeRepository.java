package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Make;
import stepan.lys.copart.model.Type;

import java.util.List;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {

    List<Type> findAllByVehicles_Body_Type(String body);

}
