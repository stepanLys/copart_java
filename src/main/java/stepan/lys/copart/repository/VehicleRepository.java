package stepan.lys.copart.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Model;
import stepan.lys.copart.model.Vehicle;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long>, JpaSpecificationExecutor<Vehicle> {

    Page<Vehicle> findAllByMakeLike(String make, Pageable pageable);

    List<Vehicle> findAllByType_VehicleType(String type);

    List<Vehicle> findAllByMake_NameAndType_VehicleType(String make, String type);

    List<Vehicle> findAllByMake_Id(Long makeId);
}
