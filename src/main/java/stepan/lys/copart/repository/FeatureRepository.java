package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Feature;

@Repository
public interface FeatureRepository extends JpaRepository<Feature, Long> {
}
