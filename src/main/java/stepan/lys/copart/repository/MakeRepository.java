package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Make;

import java.util.List;

@Repository
public interface MakeRepository extends JpaRepository<Make, Long> {
}
