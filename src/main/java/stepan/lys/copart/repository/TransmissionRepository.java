package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Transmission;

@Repository
public interface TransmissionRepository extends JpaRepository<Transmission, Long> {
}
