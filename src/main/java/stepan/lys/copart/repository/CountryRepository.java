package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
