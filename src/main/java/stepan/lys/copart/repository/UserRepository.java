package stepan.lys.copart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stepan.lys.copart.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
