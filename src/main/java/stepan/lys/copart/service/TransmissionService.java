package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.TransmissionRequest;
import stepan.lys.copart.dto.response.TransmissionResponse;
import stepan.lys.copart.model.Transmission;

public interface TransmissionService extends CrudOperations<TransmissionRequest, TransmissionResponse, Transmission> {
}
