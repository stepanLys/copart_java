package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.CountryRequest;
import stepan.lys.copart.dto.response.CountryResponse;
import stepan.lys.copart.model.Country;

public interface CountryService extends CrudOperations<CountryRequest, CountryResponse, Country> {
}
