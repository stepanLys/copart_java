package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.LocationRequest;
import stepan.lys.copart.dto.response.LocationResponse;
import stepan.lys.copart.model.Location;

import java.util.List;

public interface LocationService extends CrudOperations<LocationRequest, LocationResponse, Location> {

    List<LocationResponse> getByCountry(String name);
}
