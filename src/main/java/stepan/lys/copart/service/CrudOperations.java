package stepan.lys.copart.service;

import java.util.List;

public interface CrudOperations<Request, Response, Entity> {

    void save(Request request);

    Response getById(Long id);

    List<Response> getAll();

    void update(Long id, Request request);

    void delete(Long id);

    Entity getEntityObjectById(Long id);
}
