package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.BodyRequest;
import stepan.lys.copart.dto.response.BodyResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Body;
import stepan.lys.copart.repository.BodyRepository;
import stepan.lys.copart.service.BodyService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BodyServiceImpl implements BodyService {

    private final BodyRepository bodyRepository;

    @Autowired
    public BodyServiceImpl(BodyRepository bodyRepository) {
        this.bodyRepository = bodyRepository;
    }

    @Override
    public void save(BodyRequest request) {
        Body body = new Body();
        body.setType(request.getType());
        bodyRepository.save(body);
    }

    @Override
    public BodyResponse getById(Long id) {
        return new BodyResponse(getEntityObjectById(id));
    }

    @Override
    public List<BodyResponse> getAll() {
        return bodyRepository.findAll()
                .stream()
                .map(BodyResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, BodyRequest request) {
        Body body = getEntityObjectById(id);
        body.setType(request.getType());
        bodyRepository.save(body);
    }

    @Override
    public void delete(Long id) {
        bodyRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Body getEntityObjectById(Long id) {
        return bodyRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Body with id: " + id + " not found"));
    }
}
