package stepan.lys.copart.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import stepan.lys.copart.dto.request.FilterVehicleRequest;
import stepan.lys.copart.dto.request.LotRequest;
import stepan.lys.copart.dto.request.VehicleRequest;
import stepan.lys.copart.dto.response.*;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Lot;
import stepan.lys.copart.model.Vehicle;
import stepan.lys.copart.repository.LotRepository;
import stepan.lys.copart.repository.VehicleRepository;
import stepan.lys.copart.service.*;
import stepan.lys.copart.specification.VehicleSpecification;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.nio.file.Paths.get;

@Log4j2
@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final EngineService engineService;
    private final TransmissionService transmissionService;
    private final MakeService makeService;
    private final ModelService modelService;
    private final TypeService typeService;
    private final BodyService bodyService;
    private final LocationService locationService;
    private final UserService userService;
    private final DamageService damageService;
    private final FeatureService featureService;
    private final LotService lotService;
    private final LotRepository lotRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository, EngineService engineService, TransmissionService transmissionService, MakeService makeService, ModelService modelService, TypeService typeService, BodyService bodyService, LocationService locationService, UserService userService, DamageService damageService, FeatureService featureService, LotService lotService, LotRepository lotRepository) {
        this.vehicleRepository = vehicleRepository;
        this.engineService = engineService;
        this.transmissionService = transmissionService;
        this.makeService = makeService;
        this.modelService = modelService;
        this.typeService = typeService;
        this.bodyService = bodyService;
        this.locationService = locationService;
        this.userService = userService;
        this.damageService = damageService;
        this.featureService = featureService;
        this.lotService = lotService;
        this.lotRepository = lotRepository;
    }

    @Override
    public void save(VehicleRequest request) {
        Vehicle vehicle = new Vehicle();
        vehicleRepository.save(vehicle);
    }

    @Transactional
    @Override
    public VehicleResponse save(VehicleRequest vehicleRequest, MultipartFile file) throws IOException {
        Vehicle vehicle = new Vehicle();
        setVehicle(vehicleRequest, vehicle);

        setImage(file, vehicle);

        Lot lot = new Lot();
        lot.setStartBid(vehicleRequest.getStartBid());
        lot.setCurrentBid(vehicleRequest.getStartBid());
        lot.setStartDateTime(LocalDateTime.now());
        lot.setFinishDateTime(lot.getStartDateTime().plusDays(10));

        lotRepository.save(lot);


        vehicle.setLot(lotService.getEntityObjectById(lot.getId()));


        vehicleRepository.save(vehicle);
        return new VehicleResponse(vehicle);
    }

    @Transactional
    @Override
    public VehicleResponse update(Long id, VehicleRequest request, MultipartFile file) throws IOException {
        Vehicle vehicle = getEntityObjectById(id);
        setVehicle(request, vehicle);

        setImage(file, vehicle);

        vehicleRepository.save(vehicle);

        return new VehicleResponse(vehicle);
    }

    @Override
    public void update(Long id, VehicleRequest request) {
        Vehicle vehicle = getEntityObjectById(id);
        Lot lot = lotService.getEntityObjectById(vehicle.getLot().getId());

        vehicle.setLot(lot);
        vehicleRepository.save(vehicle);
    }

    @Override
    public VehicleResponse getById(Long id) {
        return new VehicleResponse(getEntityObjectById(id));
    }

    @Override
    public List<VehicleResponse> getAll() {
        return vehicleRepository.findAll()
                .stream()
                .map(VehicleResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public DataResponse<VehicleResponse> getAll(Integer page, Integer size,
                                                String sortBy, Sort.Direction direction,
                                                String name) {

        Sort sort = Sort.by(direction, sortBy.isEmpty() ? "id" : sortBy);
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<Vehicle> vehiclePage;
        if (name != null) {
            vehiclePage = vehicleRepository.findAllByMakeLike("%" + name + "%", pageRequest);
        } else {
            vehiclePage = vehicleRepository.findAll(pageRequest);
        }
        return new DataResponse<>(
                vehiclePage.getContent()
                        .stream().map(VehicleResponse::new)
                        .collect(Collectors.toList())
                , vehiclePage);

    }

    @Override
    public DataResponse<VehicleResponse> filter(FilterVehicleRequest filterVehicleRequest,
                                                Integer page, Integer size,
                                                String sortBy, Sort.Direction direction) {

        Sort sort = Sort.by(direction, sortBy);
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        VehicleSpecification vehicleSpecification = new VehicleSpecification(filterVehicleRequest);

        Page<Vehicle> vehiclePage = vehicleRepository.findAll(vehicleSpecification, pageRequest);


        return new DataResponse<>(
                vehiclePage.getContent()
                        .stream()
                        .map(VehicleResponse::new)
                        .collect(Collectors.toList())
                , vehiclePage);
    }

    @Override
    public List<VehicleResponse> filter(FilterVehicleRequest filterVehicleRequest) {
        VehicleSpecification vehicleSpecification = new VehicleSpecification(filterVehicleRequest);
        return vehicleRepository.findAll(vehicleSpecification)
                .stream()
                .map(VehicleResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        vehicleRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Vehicle getEntityObjectById(Long id) {
        return vehicleRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Vehicle with id: " + id + " not found"));
    }

    @Override
    public List<VehicleResponse> getByType(String type) {
        return vehicleRepository.findAllByType_VehicleType(type)
                .stream()
                .map(VehicleResponse::new)
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<VehicleResponse> getVehicleResponseByMakeAndType(String make, String type) {
        return vehicleRepository.findAllByMake_NameAndType_VehicleType(make, type)
                .stream()
                .map(VehicleResponse::new)
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<ModelResponse> getModelResponseByMakeAndType(String make, String type) {
        return vehicleRepository.findAllByMake_NameAndType_VehicleType(make, type)
                .stream()
                .map(Vehicle::getModel)
                .map(ModelResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<ModelResponse> getModelResponseByMake(Long makeId) {
        return vehicleRepository.findAllByMake_Id(makeId)
                .stream()
                .map(Vehicle::getModel)
                .map(ModelResponse::new)
                .collect(Collectors.toList());
    }

    private void setVehicle(VehicleRequest request, Vehicle vehicle) {
        vehicle.setYear(request.getYear());
        vehicle.setVIN(request.getVIN());
        vehicle.setOdometer(request.getOdometer());
        vehicle.setDescription(request.getDescription());

        System.out.println(request.getEngineId());

        vehicle.setEngine(engineService.getEntityObjectById(request.getEngineId()));
        vehicle.setTransmission(transmissionService.getEntityObjectById(request.getTransmissionId()));
        vehicle.setMake(makeService.getEntityObjectById(request.getMakeId()));
        vehicle.setModel(modelService.getEntityObjectById(request.getModelId()));
        vehicle.setType(typeService.getEntityObjectById(request.getTypeId()));
        vehicle.setBody(bodyService.getEntityObjectById(request.getBodyId()));
        vehicle.setLocation(locationService.getEntityObjectById(request.getLocationId()));
        vehicle.setOwner(userService.getEntityObjectById(request.getOwnerId()));

        vehicle.setDamages(request.getDamages()
                .stream()
                .map(damageService::getEntityObjectById)
                .collect(Collectors.toSet())
        );

        vehicle.setFeatures(request.getFeatures()
                .stream()
                .map(featureService::getEntityObjectById)
                .collect(Collectors.toSet())
        );

    }

    private void setImage(MultipartFile file, Vehicle vehicle) throws IOException {
        log.info("Original file name -> " + file.getOriginalFilename());
        String[] arrayForGetExpansion = Objects.requireNonNull(file.getOriginalFilename()).split("\\.");
        String expansion = arrayForGetExpansion[arrayForGetExpansion.length - 1];
        String nameOfFile = UUID.randomUUID() + "." + expansion;
        String fullPath = "src/main/resources/static/" + nameOfFile;

        vehicle.setUrlToPicture(nameOfFile);

        Files.copy(file.getInputStream(), get(fullPath), StandardCopyOption.REPLACE_EXISTING);
    }

}
