package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.ModelRequest;
import stepan.lys.copart.dto.response.ModelResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Make;
import stepan.lys.copart.model.Model;
import stepan.lys.copart.repository.MakeRepository;
import stepan.lys.copart.repository.ModelRepository;
import stepan.lys.copart.service.ModelService;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;
    private final MakeRepository makeRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository, MakeRepository makeRepository) {
        this.modelRepository = modelRepository;
        this.makeRepository = makeRepository;
    }

    @Override
    public void save(ModelRequest request) {
        Model model = new Model();
        model.setName(request.getName());
        model.setMake(makeRepository.getOne(request.getMakeId()));
        model.setYear(request.getYear());
        modelRepository.save(model);
    }

    @Override
    public ModelResponse getById(Long id) {
        return new ModelResponse(getEntityObjectById(id));
    }

    @Override
    public List<ModelResponse> getAll() {
        return modelRepository.findAll()
                .stream()
                .map(ModelResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, ModelRequest request) {
        Model model = getEntityObjectById(id);
        model.setName(request.getName());
        model.setMake(makeRepository.getOne(request.getMakeId()));
        model.setYear(request.getYear());
        modelRepository.save(model);
    }

    @Override
    public void delete(Long id) {
        modelRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Model getEntityObjectById(Long id) {
        return modelRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Model with id: " + id + " not found"));
    }

    @Override
    public List<ModelResponse> findByMake(String make) {
        return modelRepository.findAllByMake_Name(make)
                .stream()
                .map(ModelResponse::new)
                .collect(Collectors.toList());
    }
}
