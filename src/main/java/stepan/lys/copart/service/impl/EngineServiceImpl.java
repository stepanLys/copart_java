package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.EngineRequest;
import stepan.lys.copart.dto.response.EngineResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Engine;
import stepan.lys.copart.repository.EngineRepository;
import stepan.lys.copart.service.EngineService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EngineServiceImpl implements EngineService {
    
    private final EngineRepository engineRepository;

    @Autowired
    public EngineServiceImpl(EngineRepository engineRepository) {
        this.engineRepository = engineRepository;
    }

    @Override
    public List<EngineResponse> getAll() {
        return engineRepository.findAll()
                .stream()
                .map(EngineResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public EngineResponse getById(Long id) {
        return new EngineResponse(getEntityObjectById(id));
    }

    @Override
    public void save(EngineRequest engineRequest) {
        Engine engine = new Engine();
        engine.setCapacity(engineRequest.getCapacity());
        engine.setCylinder(engineRequest.getCylinder());
        engine.setEngineType(engineRequest.getEngineType());
        engine.setFuelType(engineRequest.getFuelType());
        engine.setPower(engineRequest.getPower());
        engineRepository.save(engine);
    }

    @Override
    public void delete(Long id) {
        engineRepository.delete(getEntityObjectById(id));
    }

    @Override
    public void update(Long id, EngineRequest engineRequest) {
        Engine engine = getEntityObjectById(id);

        engine.setCapacity(engineRequest.getCapacity());
        engine.setCylinder(engineRequest.getCylinder());
        engine.setEngineType(engineRequest.getEngineType());
        engine.setFuelType(engineRequest.getFuelType());
        engine.setPower(engineRequest.getPower());

        engineRepository.save(engine);
    }

    @Override
    public Engine getEntityObjectById(Long id) {
        return engineRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Engine with id: " + id + " not found"));
    }
}
