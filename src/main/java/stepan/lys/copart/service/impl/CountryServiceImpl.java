package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.CountryRequest;
import stepan.lys.copart.dto.response.CountryResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Country;
import stepan.lys.copart.repository.CountryRepository;
import stepan.lys.copart.service.CountryService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public void save(CountryRequest request) {
        Country country = new Country();
        country.setAbbreviation(request.getAbbreviation());
        country.setCountryName(request.getCountryName());
        countryRepository.save(country);
    }

    @Override
    public CountryResponse getById(Long id) {
        return new CountryResponse(getEntityObjectById(id));
    }

    @Override
    public List<CountryResponse> getAll() {
        return countryRepository.findAll()
                .stream()
                .map(CountryResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, CountryRequest request) {
        Country country = getEntityObjectById(id);
        country.setAbbreviation(request.getAbbreviation());
        country.setCountryName(request.getCountryName());
        countryRepository.save(country);

        /////update!!!
    }

    @Override
    public void delete(Long id) {
        countryRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Country getEntityObjectById(Long id) {
        return countryRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Country with id: " + id + " not found"));
    }

}
