package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.DamageRequest;
import stepan.lys.copart.dto.response.DamageResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Damage;
import stepan.lys.copart.repository.DamageRepository;
import stepan.lys.copart.service.DamageService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DamageServiceImpl implements DamageService {

    private final DamageRepository damageRepository;

    @Autowired
    public DamageServiceImpl(DamageRepository damageRepository) {
        this.damageRepository = damageRepository;
    }

    @Override
    public void save(DamageRequest request) {
        Damage damage = new Damage();
        damage.setDamage(request.getDamage());
        damageRepository.save(damage);
    }

    @Override
    public DamageResponse getById(Long id) {
        return new DamageResponse(getEntityObjectById(id));
    }

    @Override
    public List<DamageResponse> getAll() {
        return damageRepository.findAll()
                .stream()
                .map(DamageResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, DamageRequest request) {
        Damage damage = getEntityObjectById(id);
        damage.setDamage(request.getDamage());
        damageRepository.save(damage);
    }

    @Override
    public void delete(Long id) {
        damageRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Damage getEntityObjectById(Long id) {
        return damageRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Damage with id: " + id + " not found"));
    }
}
