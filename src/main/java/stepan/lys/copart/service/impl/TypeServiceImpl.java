package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.TypeRequest;
import stepan.lys.copart.dto.response.TypeResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Type;
import stepan.lys.copart.repository.TypeRepository;
import stepan.lys.copart.service.TypeService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeServiceImpl implements TypeService {
   
    private final TypeRepository typeRepository;

    @Autowired
    public TypeServiceImpl(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    @Override
    public void save(TypeRequest request) {
        Type type = new Type();
        type.setVehicleType(request.getVehicleType());
        typeRepository.save(type);
    }

    @Override
    public TypeResponse getById(Long id) {
        return new TypeResponse(getEntityObjectById(id));
    }

    @Override
    public List<TypeResponse> getAll() {
        return typeRepository.findAll()
                .stream()
                .map(TypeResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, TypeRequest request) {
        Type type = getEntityObjectById(id);
        type.setVehicleType(request.getVehicleType());
        typeRepository.save(type);
    }

    @Override
    public void delete(Long id) {
        typeRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Type getEntityObjectById(Long id) {
        return typeRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Type with id: " + id + " not found"));
    }

    @Override
    public List<Type> findAllByVehicles_Empty_Body(String body) {
        return typeRepository.findAllByVehicles_Body_Type(body);
    }
}
