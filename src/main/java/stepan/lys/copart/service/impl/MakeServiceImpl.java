package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.MakeRequest;
import stepan.lys.copart.dto.response.MakeResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Make;
import stepan.lys.copart.repository.CountryRepository;
import stepan.lys.copart.repository.MakeRepository;
import stepan.lys.copart.service.MakeService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MakeServiceImpl implements MakeService {
    
    private final MakeRepository makeRepository;
    private final CountryRepository countryRepository;

    @Autowired
    public MakeServiceImpl(MakeRepository makeRepository, CountryRepository countryRepository) {
        this.makeRepository = makeRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public void save(MakeRequest request) {
        Make make = new Make();
        make.setName(request.getName());
        make.setCountry(countryRepository.getOne(request.getCountryId()));
        makeRepository.save(make);
    }

    @Override
    public MakeResponse getById(Long id) {
        return new MakeResponse(getEntityObjectById(id));
    }

    @Override
    public List<MakeResponse> getAll() {
        return makeRepository.findAll()
                .stream()
                .map(MakeResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, MakeRequest request) {
        Make make = getEntityObjectById(id);
        make.setName(request.getName());
        make.setCountry(countryRepository.getOne(request.getCountryId()));
        makeRepository.save(make);
    }

    @Override
    public void delete(Long id) {
        makeRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Make getEntityObjectById(Long id) {
        return makeRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Make with id: " + id + " not found"));
    }
}
