package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.UserRequest;
import stepan.lys.copart.dto.response.UserResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Lot;
import stepan.lys.copart.model.User;
import stepan.lys.copart.repository.LocationRepository;
import stepan.lys.copart.repository.LotRepository;
import stepan.lys.copart.repository.UserRepository;
import stepan.lys.copart.service.UserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final LotRepository lotRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, LocationRepository locationRepository, LotRepository lotRepository) {
        this.userRepository = userRepository;
        this.lotRepository = lotRepository;
    }

    @Override
    public void save(UserRequest request) {
        User user = new User();
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.setPhone(request.getPhone());
        userRepository.save(user);
    }

    @Override
    public UserResponse getById(Long id) {
        return new UserResponse(getEntityObjectById(id));
    }

    @Override
    public List<UserResponse> getAll() {
        return userRepository.findAll()
                .stream()
                .map(UserResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, UserRequest request) {
        User user = getEntityObjectById(id);
        Set<Lot> lots = user.getLots();
        lots.add(lotRepository.getOne(request.getLotId()));

        user.setName(request.getName());
        user.setSurname(request.getSurname());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.setPhone(request.getPhone());

        user.setLots(lots);

        userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(getEntityObjectById(id));
    }

    @Override
    public User getEntityObjectById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("User with id: " + id + " not found"));
    }
}
