package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.TransmissionRequest;
import stepan.lys.copart.dto.response.TransmissionResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Transmission;
import stepan.lys.copart.repository.TransmissionRepository;
import stepan.lys.copart.service.TransmissionService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransmissionServiceImpl implements TransmissionService {

    private final TransmissionRepository transmissionRepository;

    @Autowired
    public TransmissionServiceImpl(TransmissionRepository transmissionRepository) {
        this.transmissionRepository = transmissionRepository;
    }

    @Override
    public void save(TransmissionRequest request) {
        Transmission transmission = new Transmission();
        transmission.setGear(request.getGear());
        transmission.setType(request.getType());
        transmissionRepository.save(transmission);
    }

    @Override
    public TransmissionResponse getById(Long id) {
        return new TransmissionResponse(getEntityObjectById(id));
    }

    @Override
    public List<TransmissionResponse> getAll() {
        return transmissionRepository.findAll()
                .stream()
                .map(TransmissionResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, TransmissionRequest request) {
        Transmission transmission = getEntityObjectById(id);
        transmission.setGear(request.getGear());
        transmission.setType(request.getType());
        transmissionRepository.save(transmission);
    }

    @Override
    public void delete(Long id) {
        transmissionRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Transmission getEntityObjectById(Long id) {
        return transmissionRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Transmission with id: " + id + " not found"));
    }
}
