package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.LotRequest;
import stepan.lys.copart.dto.response.LotResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Lot;
import stepan.lys.copart.repository.LotRepository;
import stepan.lys.copart.service.LotService;
import stepan.lys.copart.service.VehicleService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LotServiceImpl implements LotService {

    private final LotRepository lotRepository;

    @Autowired
    public LotServiceImpl(LotRepository lotRepository) {
        this.lotRepository = lotRepository;
    }

    @Override
    public void save(LotRequest request) {
        Lot lot = new Lot();
        lot.setStartBid(request.getStartBid());
        lot.setCurrentBid(request.getCurrentBid());
        lot.setStartDateTime(LocalDateTime.now());
        lot.setFinishDateTime(lot.getStartDateTime().plusDays(request.getDayDuration()));

        lotRepository.save(lot);
    }

    @Override
    public LotResponse getById(Long id) {
        return new LotResponse(getEntityObjectById(id));
    }

    @Override
    public List<LotResponse> getAll() {
        return lotRepository.findAll()
                .stream()
                .map(LotResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, LotRequest request) throws WrongInputDataException {
        Lot lot = getEntityObjectById(id);

        if (request.getCurrentBid() > lot.getCurrentBid()) {
            lot.setCurrentBid(request.getCurrentBid());
        }
        lotRepository.save(lot);
    }

    @Override
    public void delete(Long id) {
        lotRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Lot getEntityObjectById(Long id) {
        return lotRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Lot with id: " + id + " not found"));
    }
}
