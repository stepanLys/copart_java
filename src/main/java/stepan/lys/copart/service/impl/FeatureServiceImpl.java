package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.FeatureRequest;
import stepan.lys.copart.dto.response.FeatureResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Feature;
import stepan.lys.copart.repository.FeatureRepository;
import stepan.lys.copart.service.FeatureService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeatureServiceImpl implements FeatureService {

    private final FeatureRepository featureRepository;

    @Autowired
    public FeatureServiceImpl(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }

    @Override
    public void save(FeatureRequest request) {
        Feature feature = new Feature();
        feature.setFeature(request.getFeature());
        featureRepository.save(feature);
    }

    @Override
    public FeatureResponse getById(Long id) {
        return new FeatureResponse(getEntityObjectById(id));
    }

    @Override
    public List<FeatureResponse> getAll() {
        return featureRepository.findAll()
                .stream()
                .map(FeatureResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, FeatureRequest request) {
        Feature feature = getEntityObjectById(id);
        feature.setFeature(request.getFeature());
        featureRepository.save(feature);
    }

    @Override
    public void delete(Long id) {
        featureRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Feature getEntityObjectById(Long id) {
        return featureRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Feature with id: " + id + " not found"));
    }
}
