package stepan.lys.copart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stepan.lys.copart.dto.request.LocationRequest;
import stepan.lys.copart.dto.response.LocationResponse;
import stepan.lys.copart.exception.WrongInputDataException;
import stepan.lys.copart.model.Location;
import stepan.lys.copart.repository.CountryRepository;
import stepan.lys.copart.repository.LocationRepository;
import stepan.lys.copart.service.LocationService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;
    private final CountryRepository countryRepository;

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository, CountryRepository countryRepository) {
        this.locationRepository = locationRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public void save(LocationRequest request) {
        Location location = new Location();
        location.setCountry(countryRepository.getOne(request.getCountryId()));
        location.setCity(request.getCity());
        location.setPostalCode(request.getPostalCode());
        locationRepository.save(location);
    }

    @Override
    public LocationResponse getById(Long id) {
        return new LocationResponse(getEntityObjectById(id));
    }

    @Override
    public List<LocationResponse> getAll() {
        return locationRepository.findAll()
                .stream()
                .map(LocationResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<LocationResponse> getByCountry(String name) {
        return locationRepository.findAllByCountry_CountryName(name)
                .stream()
                .map(LocationResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Long id, LocationRequest request) {
        Location location = getEntityObjectById(id);
        location.setCountry(countryRepository.getOne(request.getCountryId()));
        location.setCity(request.getCity());
        location.setPostalCode(request.getPostalCode());
        locationRepository.save(location);
    }

    @Override
    public void delete(Long id) {
        locationRepository.delete(getEntityObjectById(id));
    }

    @Override
    public Location getEntityObjectById(Long id) {
        return locationRepository.findById(id)
                .orElseThrow(() -> new WrongInputDataException("Location with id: " + id + " not found"));
    }
}
