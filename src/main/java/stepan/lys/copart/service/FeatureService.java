package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.FeatureRequest;
import stepan.lys.copart.dto.response.FeatureResponse;
import stepan.lys.copart.model.Feature;

public interface FeatureService extends CrudOperations<FeatureRequest, FeatureResponse, Feature> {
}
