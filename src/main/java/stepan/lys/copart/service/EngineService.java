package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.EngineRequest;
import stepan.lys.copart.dto.response.EngineResponse;
import stepan.lys.copart.model.Engine;

public interface EngineService extends CrudOperations<EngineRequest, EngineResponse, Engine> {

}
