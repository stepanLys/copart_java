package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.DamageRequest;
import stepan.lys.copart.dto.response.DamageResponse;
import stepan.lys.copart.model.Damage;

public interface DamageService extends CrudOperations<DamageRequest, DamageResponse, Damage> {
}
