package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.UserRequest;
import stepan.lys.copart.dto.response.UserResponse;
import stepan.lys.copart.model.User;

public interface UserService extends CrudOperations<UserRequest, UserResponse, User> {
}
