package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.ModelRequest;
import stepan.lys.copart.dto.response.ModelResponse;
import stepan.lys.copart.model.Model;

import java.util.List;

public interface ModelService extends CrudOperations<ModelRequest, ModelResponse, Model> {

    List<ModelResponse> findByMake(String make);

}
