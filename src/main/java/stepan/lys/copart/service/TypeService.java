package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.TypeRequest;
import stepan.lys.copart.dto.response.TypeResponse;
import stepan.lys.copart.model.Type;

import java.util.List;

public interface TypeService extends CrudOperations<TypeRequest, TypeResponse, Type> {

    List<Type> findAllByVehicles_Empty_Body(String body);
}
