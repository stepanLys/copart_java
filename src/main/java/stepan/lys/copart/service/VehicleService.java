package stepan.lys.copart.service;

import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;
import stepan.lys.copart.dto.request.FilterVehicleRequest;
import stepan.lys.copart.dto.request.VehicleRequest;
import stepan.lys.copart.dto.response.DataResponse;
import stepan.lys.copart.dto.response.ModelResponse;
import stepan.lys.copart.dto.response.VehicleResponse;
import stepan.lys.copart.model.Vehicle;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface VehicleService extends CrudOperations<VehicleRequest, VehicleResponse, Vehicle> {

    DataResponse<VehicleResponse> getAll(Integer page,
                                         Integer size,
                                         String sortBy, Sort.Direction direction,
                                         String name);

    DataResponse<VehicleResponse> filter(FilterVehicleRequest filterVehicleRequest,
                                         Integer page, Integer size,
                                         String sortBy, Sort.Direction direction);

    VehicleResponse save(VehicleRequest vehicleRequest, MultipartFile file) throws IOException;

    VehicleResponse update(Long id, VehicleRequest vehicleRequest, MultipartFile file) throws IOException;

    List<VehicleResponse> filter(FilterVehicleRequest filterVehicleRequest);

    List<VehicleResponse> getByType(String type);

    List<VehicleResponse> getVehicleResponseByMakeAndType(String make, String type);

    List<ModelResponse> getModelResponseByMakeAndType(String make, String type);

    List<ModelResponse> getModelResponseByMake(Long makeId);

}
