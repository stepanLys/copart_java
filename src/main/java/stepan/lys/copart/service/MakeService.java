package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.MakeRequest;
import stepan.lys.copart.dto.response.MakeResponse;
import stepan.lys.copart.model.Make;

public interface MakeService extends CrudOperations<MakeRequest, MakeResponse, Make> {
}
