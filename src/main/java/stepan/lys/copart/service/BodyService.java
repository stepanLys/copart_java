package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.BodyRequest;
import stepan.lys.copart.dto.response.BodyResponse;
import stepan.lys.copart.model.Body;

public interface BodyService extends CrudOperations<BodyRequest, BodyResponse, Body> {
}
