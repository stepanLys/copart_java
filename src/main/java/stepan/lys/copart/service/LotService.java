package stepan.lys.copart.service;

import stepan.lys.copart.dto.request.LotRequest;
import stepan.lys.copart.dto.response.LotResponse;
import stepan.lys.copart.model.Lot;

public interface LotService extends CrudOperations<LotRequest, LotResponse, Lot> {
}
